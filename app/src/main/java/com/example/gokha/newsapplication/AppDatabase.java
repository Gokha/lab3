package com.example.gokha.newsapplication;

/**
 * Created by Zhanat on 02.10.17.
 */

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
@Database(entities = {NewsRoom.class, CategoryRoom.class}, version = 1)

public abstract class AppDatabase extends RoomDatabase {
    public abstract NewsDao newsDao();
    public abstract CategoryDao categoryDao();
}
