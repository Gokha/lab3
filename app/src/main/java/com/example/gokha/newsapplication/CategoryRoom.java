package com.example.gokha.newsapplication;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Zhanat on 02.10.17.
 */

@Entity(tableName = "categories")
public class CategoryRoom {

    @PrimaryKey
    @ColumnInfo(name = "categoryid")
    private String mId;


    @ColumnInfo(name = "name")
    String name;


    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
