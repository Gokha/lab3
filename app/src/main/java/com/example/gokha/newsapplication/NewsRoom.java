package com.example.gokha.newsapplication;

/**
 * Created by Zhanat on 02.10.17.
 */

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "news")
public class NewsRoom implements Serializable{

    @PrimaryKey
    @ColumnInfo(name = "newsid")
    private String mId;


    @ColumnInfo(name = "title")
    String title;


    @ColumnInfo(name = "date")
    String date;

    @ColumnInfo(name = "text")
    String text;


    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
