package com.example.gokha.newsapplication;

/**
 * Created by Gokha on 28.09.17.
 */

import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

public class TabCategory extends Fragment{


    AppDatabase database;
    public TabCategory() {
        // Required empty public constructor
    }

    List<CategoryRoom> categoriesList = new ArrayList<>();
    MyAdapter1 myAdapter1;
    GridView gridView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = Room.databaseBuilder(getActivity().getApplicationContext(),
                AppDatabase.class, "Room.db")
                .build();


        //for (int i=0; i<20; i++){
       //     Category category = new Category();
       //     category.setName("Category # "+i);
       //     categoriesList.add(category);
       // }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.category_tab, container,
                false);
        gridView = (GridView) view.findViewById(R.id.gridView);


        new DatabaseAsync().execute();

        return view;
    }

    public void setAdapter(){
        myAdapter1 = new MyAdapter1(getContext(),categoriesList);
        gridView.setAdapter(myAdapter1);
    }

    private class DatabaseAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //Perform pre-adding operation here.
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (int i=0; i<20; i++){
                CategoryRoom categories = new CategoryRoom();
                categories.setName("Category # "+i);
                database.categoryDao().insert(categories);
            }

            categoriesList = database.categoryDao().getAll();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setAdapter();
            //To after addition operation here.
        }
    }
}
