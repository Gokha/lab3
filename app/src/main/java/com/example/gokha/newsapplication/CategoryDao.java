package com.example.gokha.newsapplication;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by Zhanat on 02.10.17.
 */
@Dao
public interface CategoryDao {
    @Query("SELECT * FROM categories")
    List<CategoryRoom> getAll();
    @Insert
    void insert(CategoryRoom categories);

    @Delete
    void delete(CategoryRoom categories);

}
