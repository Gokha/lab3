package com.example.gokha.newsapplication;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by delaroy on 2/13/17.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    List<NewsRoom> mNewsList;
    private String[] mDataset;
    Context mContext;

    public MyAdapter(Context context, List<NewsRoom> newsList) {

        mNewsList = newsList;
        //mDataset = myDataset;
        mContext = context;
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NewsRoom news = mNewsList.get(position);
        holder.setPosition(position);
        holder.mTitle.setText(news.getTitle());
        holder.mDate.setText(news.getDate());
//        holder.mText.setText(news.getText());
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //public CardView mCardView;
        TextView mTitle;
        TextView mDate;
        //        TextView mText;
        //Context mContext;
        int position;

        public MyViewHolder(View v) {
            super(v);
            mTitle = (TextView) v.findViewById(R.id.news_title);
//            mText = (TextView) v.findViewById(R.id.news_text);
            mDate = (TextView) v.findViewById(R.id.news_date);
            v.setOnClickListener(this);


            //mCardView = (CardView) v.findViewById(R.id.card_view);
            //mTextView = (TextView) v.findViewById(R.id.tv_text);

        }

        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            NewsRoom NewsObject = new NewsRoom();
            Intent intent = new Intent(mContext, NewsDetails.class);
            Log.e("News", mNewsList.get(position).getTitle());
            intent.putExtra("news", mNewsList.get(position));
            mContext.startActivity(intent);

        }

    }


    @Override
    public int getItemCount() {
        return mNewsList.size();
    }

}