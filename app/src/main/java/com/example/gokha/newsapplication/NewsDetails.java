package com.example.gokha.newsapplication;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Zhanat on 02.10.17.
 */

public class NewsDetails extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_details);
        AppDatabase database = Room.databaseBuilder(getApplicationContext().getApplicationContext(),
                AppDatabase.class, "Room.db")
                .build();

        NewsRoom news = (NewsRoom) getIntent().getExtras().getSerializable("news");

        TextView textView = (TextView) findViewById(R.id.news_title);
        TextView textView1 = (TextView) findViewById(R.id.news_date);
        TextView textView2 = (TextView) findViewById(R.id.news_text);

        textView.setText(news.getTitle());
        textView1.setText(news.getDate());
        textView2.setText(news.getText());

    }
}
