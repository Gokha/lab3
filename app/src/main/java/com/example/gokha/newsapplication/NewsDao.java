package com.example.gokha.newsapplication;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by Zhanat on 02.10.17.
 */

@Dao
public interface NewsDao {

    @Query("SELECT * FROM news")
    List<NewsRoom> getAll();
    @Insert
    void insert(NewsRoom news);

    @Delete
    void delete(NewsRoom news);
}
