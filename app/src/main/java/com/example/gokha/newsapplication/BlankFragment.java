package com.example.gokha.newsapplication;

/**
 * Created by Zhanat on 01.10.17.
 */

import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

;

public class BlankFragment extends Fragment {

    AppDatabase database;
    RecyclerView rv;
    public BlankFragment() {
        // Required empty public constructor
    }

    List<NewsRoom> newsList = new ArrayList<>();
    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = Room.databaseBuilder(getActivity().getApplicationContext(),
                AppDatabase.class, "Room.db")
                .build();



        //for (int i=0; i<20; i++){
        //    News news = new News();
        //    news.setTitle("Title "+i);
        //    news.setText("Text " +i);
        //    news.setDate("2"+i+".10.17");
        //    newsList.add(news);
        //}

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_blank, container, false);

        rv = (RecyclerView) rootView.findViewById(R.id.rv_recycler_view);
        rv.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        new DatabaseAsync().execute();
        return rootView;
    }

    public void setAdapter(){
        MyAdapter adapter = new MyAdapter(getActivity(), newsList);
        rv.setAdapter(adapter);
    }

    private class DatabaseAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //Perform pre-adding operation here.
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (int i=0; i<20; i++){
                NewsRoom news = new NewsRoom();
                news.setTitle("Title "+i);
                news.setText("Text " +i);
                news.setDate("2"+i+".10.17");
                database.newsDao().insert(news);
            }

             newsList = database.newsDao().getAll();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setAdapter();
            //To after addition operation here.
        }
    }



}